#!/usr/bin/env python3
from __future__ import annotations

import os
import pathlib
import platform
import shutil
import subprocess
import tarfile

import httpx

CURRENT_DIR = pathlib.Path(__file__).parent

def main():
	delta()
	dua()
	eza()
	git_whence()
	starship()

def delta():
	if platform.system() == 'Darwin':
		print('$ brew install git-delta')
		subprocess.run(['brew', 'install', 'git-delta'], check=True)
		return
	else:
		assert platform.system() == 'Linux'

	if subprocess.run(['dpkg', '-l', 'git-delta'], stdout=subprocess.DEVNULL).returncode == 0:
		print('git-delta package already installed')
		return

	client = httpx.Client()
	latest = gh_latest_version(client, 'dandavison', 'delta')

	version = latest['name']
	arch = get_output(['dpkg', '--print-architecture'])
	filename = f'git-delta_{version}_{arch}.deb'

	(asset,) = (asset for asset in latest['assets'] if asset['name'] == filename)
	deb_path = CURRENT_DIR / 'git-delta.deb'
	download(client, asset['browser_download_url'], deb_path)

	try:
		subprocess.run(['sudo', 'dpkg', '-i', deb_path], check=True)
	finally:
		os.unlink(deb_path)

def dua():
	if platform.system() == 'Darwin':
		print('$ brew install dua-cli')
		subprocess.run(['brew', 'install', 'dua-cli'], check=True)
		return
	else:
		assert platform.system() == 'Linux'

	dua_path = CURRENT_DIR / 'dua'
	if dua_path.exists():
		print('dua already downloaded')
		return

	client = httpx.Client()
	latest = gh_latest_version(client, 'Byron', 'dua-cli')
	version = latest['name']
	dirname = f'dua-{version}-{platform.machine()}-unknown-linux-musl'
	filename = dirname + '.tar.gz'
	(asset,) = (asset for asset in latest['assets'] if asset['name'] == filename)
	tarball_path = CURRENT_DIR / 'dua.tar.gz'
	download(client, asset['browser_download_url'], tarball_path)
	with tarfile.open(tarball_path) as tar:
		with tar.extractfile(dirname + '/dua') as binary, dua_path.open('wb') as f: # pyright: ignore[reportOptionalContextManager]
			shutil.copyfileobj(binary, f)
	os.unlink(tarball_path)
	dua_path.chmod(0o755)

def eza():
	if platform.system() == 'Darwin':
		print('$ brew install eza')
		subprocess.run(['brew', 'install', 'eza'], check=True)
		return
	else:
		assert platform.system() == 'Linux'

	if (CURRENT_DIR / 'eza').exists():
		print('eza already downloaded')
		return

	client = httpx.Client()
	url = f'https://github.com/eza-community/eza/releases/latest/download/eza_{platform.machine()}-unknown-linux-gnu.tar.gz'
	tarball_path = CURRENT_DIR / 'eza.tar.gz'
	download(client, url, tarball_path)
	with tarfile.open(tarball_path) as tar:
		tar.extract('./eza', CURRENT_DIR)
	tarball_path.unlink()

def git_whence():
	if platform.system() == 'Darwin':
		print('$ brew install raylu/formulae/git-whence')
		subprocess.run(['brew', 'install', 'raylu/formulae/git-whence'], check=True)
		return
	else:
		assert platform.system() == 'Linux'

	if (CURRENT_DIR / 'git-whence').exists():
		print('git-whence already downloaded')
		return

	client = httpx.Client()
	url = f'https://github.com/raylu/git-whence/releases/latest/download/git-whence-{platform.machine()}-unknown-linux-gnu'
	download(client, url, CURRENT_DIR / 'git-whence')
	os.chmod(CURRENT_DIR / 'git-whence', 0o755)

def starship():
	if platform.system() == 'Darwin':
		print('$ brew install starship')
		subprocess.run(['brew', 'install', 'starship'], check=True)
		return
	else:
		assert platform.system() == 'Linux'

	if (CURRENT_DIR / 'starship').exists():
		print('starship already downloaded')
		return

	client = httpx.Client()
	url = f'https://github.com/starship/starship/releases/latest/download/starship-{platform.machine()}-unknown-linux-gnu.tar.gz'
	tarball_path = CURRENT_DIR / 'starship.tar.gz'
	download(client, url, tarball_path)
	with tarfile.open(tarball_path, 'r:gz') as tar:
		tar.extract('starship', CURRENT_DIR)
	tarball_path.unlink()


def get_output(argv: list[str]) -> str:
	proc = subprocess.run(argv, check=True, capture_output=True, encoding='ascii')
	return proc.stdout.rstrip('\n')

def gh_latest_version(client: httpx.Client, org: str, repo: str) -> dict:
	r = client.get(f'https://api.github.com/repos/{org}/{repo}/releases/latest',
			headers={'Accept': 'application/vnd.github+json', 'X-GitHub-Api-Version': '2022-11-28'})
	r.raise_for_status()
	return r.json()

def download(client: httpx.Client, url: str, path: pathlib.Path) -> None:
	print('downloading', url, 'to', path)
	with client.stream('GET', url, follow_redirects=True) as r:
		r.raise_for_status()
		with path.open('wb') as f:
			for chunk in r.iter_bytes():
				f.write(chunk)

if __name__ == '__main__':
	main()
